package com.sc.ui;

import com.sc.adapter.PhotosAdapter;
import com.sc.lane.R;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class FragementPhotos extends Fragment {
	private Activity mActivity;
	private PhotosAdapter mAdapter;
	private ListView mlvPhotosList;
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		System.out.println("AAAAAAAAAA____onAttach");
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		System.out.println("AAAAAAAAAA____onCreate");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		System.out.println("AAAAAAAAAA____onCreateView");
		View view = inflater.inflate(R.layout.main_fragement_photos, container,
				false);
		mActivity = this.getActivity();
		
		mAdapter = new PhotosAdapter(mActivity);
		mlvPhotosList = (ListView)view.findViewById(R.id.lv_photos);
		mlvPhotosList.setAdapter(mAdapter);

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		System.out.println("AAAAAAAAAA____onActivityCreated");
	}

	@Override
	public void onStart() {
		super.onStart();
		System.out.println("AAAAAAAAAA____onStart");
	}

	@Override
	public void onResume() {
		super.onResume();
		System.out.println("AAAAAAAAAA____onResume");
	}

	@Override
	public void onPause() {
		super.onPause();
		System.out.println("AAAAAAAAAA____onPause");
	}

	@Override
	public void onStop() {
		super.onStop();
		System.out.println("AAAAAAAAAA____onStop");
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		System.out.println("AAAAAAAAAA____onDestroyView");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		System.out.println("AAAAAAAAAA____onDestroy");
	}

	@Override
	public void onDetach() {
		super.onDetach();
		System.out.println("AAAAAAAAAA____onDetach");
	}
}
