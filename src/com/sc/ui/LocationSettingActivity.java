package com.sc.ui;

import java.text.SimpleDateFormat;

import com.sc.lane.R;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class LocationSettingActivity extends Activity implements OnClickListener{
	private EditText metGPSInfo;
	private static final String TAG = "MainActivity";
	
	Button mbnStartService;
	Button mbnStopService;
	private LocationDataReceiver receiver ;

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		this.unregisterReceiver(receiver);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.location_setting);

		metGPSInfo = (EditText) this.findViewById(R.id.editText);
		mbnStartService = (Button) this.findViewById(R.id.start_service);
		mbnStopService = (Button) this.findViewById(R.id.stop_service);
		mbnStartService.setOnClickListener(this);
		mbnStopService.setOnClickListener(this);
		mbnStopService.setClickable(false);
		
		receiver = new LocationDataReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction("android.intent.action.loc");
		this.registerReceiver(receiver, filter);
	}
	
	@Override  
    public void onClick(View v) {  
        Intent intent = new Intent("com.sc.service.LocationService"); 
        
        switch(v.getId()){
        case R.id.start_service:
        	startService(intent);
        	mbnStartService.setClickable(false);
        	mbnStopService.setClickable(true);
        	break;
        case R.id.stop_service:
        	stopService(intent);
        	mbnStartService.setClickable(true);
        	mbnStopService.setClickable(false);
        	break;
    	default:
    		break;
        }
	}

	private void updateView(Location location) {
		if (location != null) {
			metGPSInfo.setText("设备位置信息\n\n经度：");
			metGPSInfo.append(String.valueOf(location.getLongitude()));
			metGPSInfo.append("\n纬度：");
			metGPSInfo.append(String.valueOf(location.getLatitude()));
			metGPSInfo.append("\n海拔：");
			metGPSInfo.append(String.valueOf(location.getAltitude()));
			metGPSInfo.append("\n时间：");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
			metGPSInfo.append(sdf.format(location.getTime()));
		} else {
			// 清空EditText对象
			metGPSInfo.getEditableText().clear();
		}
	}
	
	public class LocationDataReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context arg0, Intent arg1) {
			System.out.println("OnReceiver");
			
			Location loc = (Location) arg1.getParcelableExtra("loc");
			
			updateView(loc);
		}
	}
}
